// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
	//SetReplicates(true);
}

void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplose(DeltaTime);


}

void AProjectileDefault_Grenade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
			Explose();

		}
		else
		{
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (!TimerEnabled)
	{
		Explose();
	}
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	//Init Grenade
	TimerEnabled = true;
}

void AProjectileDefault_Grenade::Explose()
{
	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		InitExploseFX_Multicast(ProjectileSetting.ExploseFX);
	}
	if (ProjectileSetting.ExploseSound)
	{
		InitExploseSound_Multicast(ProjectileSetting.ExploseSound);
	}

	TArray<AActor*> IgnoredActor;
	//DrawDebugSphere(GetWorld(), GetActorLocation(), ExploseMaxDamageRadius, 26, FColor::Red, false, SphereLifeTime, 0, 1);
	//DrawDebugSphere(GetWorld(), GetActorLocation(), ExploseMinDamageRadius, 26, FColor::Yellow, false, SphereLifeTime, 0, 1);

		UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
			ProjectileSetting.ExploseMaxDamage,
			ProjectileSetting.ExploseMaxDamage * 0.2f,
			GetActorLocation(),
			ExploseMaxDamageRadius,
			ExploseMinDamageRadius,
			5,
			NULL, IgnoredActor, this, nullptr);



	this->Destroy();
}

void AProjectileDefault_Grenade::InitExploseFX_Multicast_Implementation(UParticleSystem* BoomFX)
{
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BoomFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
}

void AProjectileDefault_Grenade::InitExploseSound_Multicast_Implementation(USoundBase* BoomSound)
{
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), BoomSound, GetActorLocation());
}
