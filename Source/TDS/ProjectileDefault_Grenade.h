// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 *
 */
UCLASS()
class TDS_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void TimerExplose(float DeltaTime);

	void BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	virtual void ImpactProjectile() override;
	
		void Explose();

		UFUNCTION(NetMulticast, Reliable)
			void InitExploseFX_Multicast(UParticleSystem* BoomFX);
		UFUNCTION(NetMulticast, Reliable)
			void InitExploseSound_Multicast(USoundBase* BoomSound);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
		bool TimerEnabled = false;
	
	float TimerToExplose = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
		float TimeToExplose = 5.0f;
	
	float ExploseMaxDamageRadius = 150.0f;
	float ExploseMinDamageRadius = 300.0f;
	float SphereLifeTime = 3.0f;

};