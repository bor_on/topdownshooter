// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSCharacterHealthComponent.h"

void UTDSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{
			//FX
			UE_LOG(LogTemp, Warning, TEXT("UTDSCharacterHealthComponent::ChangeHealthValue_OnServer - Shield < 0"));
		}
	}
	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}
}

float UTDSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTDSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	ShieldChangeEvent_Multicast(Shield, ChangeValue);
	//OnShieldChange.Broadcast(Shield, ChangeValue);

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		}
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CooldownShieldTimer, this, &UTDSCharacterHealthComponent::CooldownShieldEnd, CoolownShieldRecoverTime,false);
	
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UTDSCharacterHealthComponent::CooldownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTDSCharacterHealthComponent::RecoveryShield, ShieldRecoveryRate, true);
	}
}

void UTDSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoveryValue;
	if (tmp > 100)
	{
		Shield = 100.f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	ShieldChangeEvent_Multicast(Shield, ShieldRecoveryValue);
	//OnShieldChange.Broadcast(Shield, ShieldRecoveryValue);
}

float UTDSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

void UTDSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float Shild, float Dam)
{
	OnShieldChange.Broadcast(Shild, Dam);
}
