// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Character/TDSHealthComponent.h"
#include "TDSCharacterHealthComponent.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);

/**
 * 
 */
UCLASS()
class TDS_API UTDSCharacterHealthComponent : public UTDSHealthComponent
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Health")
		FOnShieldChange OnShieldChange;

	FTimerHandle TimerHandle_CooldownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

protected:
	float Shield = 100.0f;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoveryRate = 0.1f;

		void ChangeHealthValue_OnServer(float ChangeValue) override;

	float GetCurrentShield();

	void ChangeShieldValue(float ChangeValue);
	void CooldownShieldEnd();
	void RecoveryShield();

	UFUNCTION(BlueprintCallable)
		float GetShieldValue();

	UFUNCTION(NetMulticast, Reliable, BlueprintCallable, Category = "Shield")
		void ShieldChangeEvent_Multicast(float Shild, float Dam);
};
