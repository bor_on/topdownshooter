// Fill out your copyright notice in the Description page of Project Settings.


#include "TDS_StateEffect.h"
#include "Kismet/GameplayStatics.h"
#include "TDS_IGameActor.h"
#include "Character/TDSHealthComponent.h"
#include "Net/UnrealNetwork.h"

bool UTDS_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;
	NameBone = FName("ParticleSocket");

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_AddEffect(myActor, this);
	}

	return true;
}

void UTDS_StateEffect::DestroyObject()
{
	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->Execute_RemoveEffect(myActor, this);
	}
	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}


bool UTDS_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	/*GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteOnce::DestroyObject, Timer, false);

	ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
	if (myInterface)
	{
		ParticleEmitter = myInterface->ReturnEffectLocation(ParticleEffect);
	}*/
	ExecuteOnce();

	
	return true;
}

void UTDS_StateEffect_ExecuteOnce::DestroyObject()
{
	//ParticleEmitter->DestroyComponent();
	//ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UTDS_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UTDSHealthComponent* MyHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
		if(MyHealthComp)
			{
				MyHealthComp->ChangeHealthValue_OnServer(Power);
			}
	}
	DestroyObject();
}

bool UTDS_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UTDS_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UTDS_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}


	//ITDS_IGameActor* myInterface = Cast<ITDS_IGameActor>(myActor);
		//if (myInterface)
		//{
			//USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMesh::StaticClass()));
			//if (myMesh)
			//{
			//	ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, myMesh, FName("ParticleSocket"));
			//}
			//else 
			//{
				//if(myActor->GetComponentByClass(USkeletalMesh::StaticClass()))
				//ParticleEmitter = myInterface->ReturnEffectLocation(ParticleEffect);
			//}
		//}

	return true;
}

	void UTDS_StateEffect_ExecuteTimer::DestroyObject()
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearAllTimersForObject(this);
		}

		//ParticleEmitter->DestroyComponent();
		//ParticleEmitter = nullptr;
		Super::DestroyObject();
	}

	void UTDS_StateEffect_ExecuteTimer::Execute()
	{
		if (myActor)
		{
			UTDSHealthComponent* MyHealthComp = Cast<UTDSHealthComponent>(myActor->GetComponentByClass(UTDSHealthComponent::StaticClass()));
			if (MyHealthComp)
			{
				MyHealthComp->ChangeHealthValue_OnServer(Power);
			}
		}
	}

	void UTDS_StateEffect::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
	{
		Super::GetLifetimeReplicatedProps(OutLifetimeProps);

		DOREPLIFETIME(UTDS_StateEffect, NameBone);

	}
